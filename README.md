# go2rpm

go2rpm is a Python application designed to assist in the creation of RPM
specfiles for Go (Golang) projects.
It automates the process of
fetching project details, determining dependencies, and generating a specfile.

## Features

- **Automatic Specfile Generation**: go2rpm automatically generates RPM
  specfiles based on the Go project's details and structure.
- **Support for Various Forges**: go2rpm works with different code hosting
  platforms such as GitHub, GitLab, Bitbucket, Pagure, and Gitea.
- **Up to commit level spec generation**: go2rpm can create a spec file based
  on a project's latest release or tag (default) or a custom version, tag, or commit.

## Usage

To use go2rpm, execute the application with the necessary parameters:

``` shell
go2rpm [OPTIONS] GO_IMPORT_PATH
```

### Options

- **-r, --rpmautospec**: Use autorelease and autochangelog features (default)
- **-n, --no-rpmautospec**: Use static release and changelog instead of rpmautospec
- **-L, --use-new-versioning**: Enable new naming scheme for
   versioned compat packages (default)
- **--no-use-new-versioning**: Use older naming scheme for versioned compat packages
- **--no-auto-changelog-entry**: Do not generate a changelog entry
- **--dynamic-buildrequires**: Use dynamic BuildRequires feature (default)
- **-R, --no-dynamic-buildrequires**: Do not use dynamic BuildRequires feature
- **-C, --clean**: Clean cache for chosen Go import path (default)
- **--no-clean**: Do not clean the cache
- **--clean-all**: Clean all cached Go imports
- **-d, --create-directory**: Save the final specfile output to NAME/NAME.spec
- **--name**: Use a specific name for the spec file
- **--print-name**: Print the generated package name and exit
- **-q, --no-spec-warnings**: Exclude warning comments from generated specfile
- **-t, --tag**: Use especified package tag
- **-a, --altipaths**: Include alternate import paths
- **-c, --commit**: Use especified package commit
- **-f, --forge**: Forge URLs
- **-v, --version**: Use especified package versions
- **--stdout**: Print spec into stdout

### Examples

``` shell
# Generate specfile a project hosted on GitHub
go2rpm github.com/rhysd/actionlint

# Generate specfile for a project where import path doesn't match the repository
go2rpm --forge https://github.com/kubernetes/api k8s.io/api

# Generate specfile for a project using a specific commit
go2rpm --commit c2c7a15d6c994356c68dc7a14868c3519836286b --forge 'https://git.sr.ht/~emersion/go-scfg' 'git.sr.ht/~emersion/go-scfg'

# Generate specfile for a project using a specific version
go2rpm -v 2.1.0 github.com/hashicorp/hcl/v2

# Generate specfile with a custom name. This is useful for application packages.
go2rpm --name gh -f https://github.com/cli/cli github.com/cli/cli/v2
```

## Requirements

- Python 3
- Git
- Askalono (a license detection tool)
- Aiohttp (for asynchronous HTTP requests)

## License

This application is licensed under the [MIT License](LICENSE).
Feel free to modify and distribute it in accordance with the license terms.
