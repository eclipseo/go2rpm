# NEWS

## 1.13.1 - 2024-06-14 <a id='1.13.1'></a>

### Added

- Publish go2rpm to PyPI

### Fixed

- templates vendor: adjust go-vendor-tools–generated comment

## 1.13.0 - 2024-04-10 <a id='1.13.0'></a>

### Added

- `templates`: add consistent sorting to ensure that specfile generation is
  deterministic
- `vendor`: add `--compression` and `--compresslevel` flags
- `vendor`: add informational comment above generated license
- `vendor`: use `bz2` as default compression to match `go-vendor-tools`
- `vendor`; `packaging`: bump minimum go-vendor-tools version

## 1.12.0 - 2024-03-28 <a id='1.12.0'></a>

### Added

- `doc`: extend README file (@mikelolasagasti)
- **`main`: add `vendor` profile and integrate go-vendor-tools**
- `main`: add `--no-clean flag` and use it in tests
- `main`: add `-V/--go2rpm-version` flag
- **`main`: add `--download` flag to download sources**
- `tests`: add basic pytest unit tests

### Changed

- `init`: correct `__version__` constant
- `packaging`: switch to `flit_core` and adopt PEP 621 metadata

### Fixed

- `cli`: print error message to stderr
- **`rpmname`: fix `use_new_versioning` regex for multi-number suffixes**

## 1.11.1 - 2024-04-08 <a id='1.11.1'></a>

This is an **out-of-band release** that includes changes from 1.12.0.

### Fixed

- **`rpmname`: fix use_new_versioning regex**
- `.gitignore`: add missing setuptools build directories
- `init`: correct `__version__` constant
- `setup.py`: fix metadata for upload to PyPI
